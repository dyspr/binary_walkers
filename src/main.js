var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var mazeSize = 15
var maze = create2DArray(mazeSize, mazeSize, 0, false)
var initSize = 0.05
var frame = 0
var speed = 30
var walkers = []
var newRow = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
  newRow.push(1)
  for (var i = 1; i < mazeSize - 1; i++) {
    newRow.push(Math.floor(Math.random() * 2))
  }
  newRow.push(0)
  for (var i = 0; i < mazeSize - 1; i++) {
    walkers.push([i, Math.floor(mazeSize * 0.5)])
  }
  for (var i = 0; i < mazeSize; i++) {
    maze[0][i] = 1
    maze[mazeSize - 1][i] = 0
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < maze.length; i++) {
    for (var j = 0; j < maze[i].length; j++) {
      if (maze[i][j] === 0) {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(mazeSize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(mazeSize * 0.5)) * boardSize * initSize - sin((frame / (speed * 2)) * Math.PI) * boardSize * initSize)
        rotate(Math.PI * 0.5)
        drawTile(boardSize * initSize, 8 + (255 - 8) * (1 / mazeSize) * j)
        pop()
      } else {
        push()
        translate(windowWidth * 0.5 + (i - Math.floor(mazeSize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (j - Math.floor(mazeSize * 0.5)) * boardSize * initSize - sin((frame / (speed * 2)) * Math.PI) * boardSize * initSize)
        drawTile(boardSize * initSize, 8 + (255 - 8) * (1 / mazeSize) * j)
        pop()
      }
    }
  }

  for (var i = 0; i < newRow.length; i++) {
    if (newRow[i] === 0) {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(mazeSize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (Math.floor((mazeSize + 1) * 0.5)) * boardSize * initSize - sin((frame / (speed * 2)) * Math.PI) * boardSize * initSize)
      rotate(Math.PI * 0.5)
      drawTile(boardSize * initSize, 8 + (255 - 8) * (frame / speed))
      pop()
    } else {
      push()
      translate(windowWidth * 0.5 + (i - Math.floor(mazeSize * 0.5)) * boardSize * initSize, windowHeight * 0.5 + (Math.floor((mazeSize + 1) * 0.5)) * boardSize * initSize - sin((frame / (speed * 2)) * Math.PI) * boardSize * initSize)
      drawTile(boardSize * initSize, 8 + (255 - 8) * (frame / speed))
      pop()
    }
  }

  for (var i = 0; i < walkers.length; i++) {
    push()
    translate(windowWidth * 0.5 + (walkers[i][0] - Math.floor(mazeSize * 0.5)) * boardSize * initSize + boardSize * initSize * 0.5, windowHeight * 0.5 + (walkers[i][1] - Math.floor(mazeSize * 0.5)) * boardSize * initSize - sin((frame / (speed * 2)) * Math.PI) * boardSize * initSize * 0)
    fill(255)
    noStroke()
    rotate(Math.PI * 0.25)
    rect(0, 0, boardSize * initSize * 0.25, boardSize * initSize * 0.25)
    pop()
  }

  // frame += deltaTime * 0.025
  // if (frame > speed) {
  //   frame = 0
  //
  //   for (var i = 0; i < maze.length; i++) {
  //     maze[i].shift()
  //     maze[i].push(newRow[i])
  //   }
  //   newRow = []
  //   newRow.push(1)
  //   for (var i = 1; i < mazeSize - 1; i++) {
  //     newRow.push(Math.floor(Math.random() * 2))
  //   }
  //   newRow.push(0)
  // }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function movementChecker(walkers, array) {
  var movement = []
  for (var i = 0; i < walkers.length; i++) {
    // if (array[walkers[i][0]][walkers[i][1]] === 1 && array[walkers[i][0] + 1][walkers[i][1]] === 0) {
    //   movement.push(false)
    // }
    // if (array[walkers[i][0]][walkers[i][1]] === 0 && array[walkers[i][0] + 1][walkers[i][1]] === 1) {
    //   if (array[walkers[i][0]][walkers[i][1] + 1] === 1 && array[walkers[i][0] + 1][walkers[i][1] + 1] === 0) {
    //     movement.push([0, 0])
    //   }
    //   if (array[walkers[i][0]][walkers[i][1] + 1] === 1 && array[walkers[i][0] + 1][walkers[i][1] + 1] === 1) {
    //     movement.push([0, 0])
    //   }
    //   if (array[walkers[i][0]][walkers[i][1] + 1] === 0 && array[walkers[i][0] + 1][walkers[i][1] + 1] === 0) {
    //     movement.push([0, 0])
    //   }
    //   if (array[walkers[i][0]][walkers[i][1] + 1] === 0 && array[walkers[i][0] + 1][walkers[i][1] + 1] === 1) {
    //     movement.push([0, 0])
    //   }
    // }
    // if (walkers[i][0] % 2 === 0) {
    //   if (array[walkers[i][0]][walkers[i][1]] === 1 && array[walkers[i][0] + 1][walkers[i][1]] === 0) {
    //     movement.push(false)
    //   }
    //   if (array[walkers[i][0]][walkers[i][1]] === 1 && array[walkers[i][0] + 1][walkers[i][1]] === 1) {
    //     if (array[walkers[i][0] + 1][walkers[i][1] + 1] === 1) {
    //       movement.push([1, 1])
    //     }
    //     if (array[walkers[i][0] + 1][walkers[i][1] + 1] === 0) {
    //       movement.push([1, -1])
    //     }
    //   }
    //   if (array[walkers[i][0]][walkers[i][1]] === 0 && array[walkers[i][0] + 1][walkers[i][1]] === 0) {
    //     if (array[walkers[i][0]][walkers[i][1] + 1] === 0) {
    //       movement.push([-1, -1])
    //     }
    //     if (array[walkers[i][0]][walkers[i][1] + 1] === 1) {
    //       movement.push([-1, 1])
    //     }
    //   }
    // } else {
    //   if (array[walkers[i][0]][walkers[i][1] + 1] === 1 && array[walkers[i][0] + 1][walkers[i][1] + 1] === 0) {
    //     movement.push([0, 0])
    //   }
    //   if (array[walkers[i][0]][walkers[i][1]] === 1 && array[walkers[i][0] + 1][walkers[i][1]] === 0) {
    //     movement.push(false)
    //   }
    //
    // }
  }
  return movement
}

function drawTile(size, col) {
  fill(col)
  noStroke()
  beginShape()
  vertex(-(1 / 2) * size, -(1 / 2) * size)
  vertex(-(1 / 3) * size, -(1 / 2) * size)
  vertex((1 / 2) * size, (1 / 3) * size)
  vertex((1 / 2) * size, (1 / 2) * size)
  vertex((1 / 3) * size, (1 / 2) * size)
  vertex(-(1 / 2) * size, -(1 / 3) * size)
  endShape()
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = [i, j]
      } else {
        columns[j] = Math.floor(Math.random() * 2)
      }
    }
    array[i] = columns
  }
  return array
}
